package com.epam.cloudx;

import software.amazon.awssdk.regions.Region;

public final class Config {
    public static final Region REGION = Region.EU_CENTRAL_1;

    public static final String NOTIFICATIONS_SQS_QUEUE_URL =
            "https://sqs.eu-central-1.amazonaws.com/524876846205/CloudxAwsApp-uploads-notification-queue";

    public static final String NOTIFICATIONS_SNS_TOPIC_ARN =
            "arn:aws:sns:eu-central-1:524876846205:CloudxAwsApp-uploads-notification-topic";

    public static final int RECEIVE_MAX_NUMBER = 10;

    public static final int RECEIVE_TIMEOUT_SECONDS = 3;

    private Config() {
    }
}
