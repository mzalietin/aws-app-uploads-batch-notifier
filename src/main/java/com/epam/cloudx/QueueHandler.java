package com.epam.cloudx;

import static com.epam.cloudx.Config.NOTIFICATIONS_SNS_TOPIC_ARN;
import static com.epam.cloudx.Config.NOTIFICATIONS_SQS_QUEUE_URL;
import static com.epam.cloudx.Config.RECEIVE_MAX_NUMBER;
import static com.epam.cloudx.Config.RECEIVE_TIMEOUT_SECONDS;
import static com.epam.cloudx.Config.REGION;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.epam.cloudx.dto.ImageInfo;
import com.epam.cloudx.dto.Notification;
import com.epam.cloudx.dto.rs.ApiGatewayCompatibleResponse;
import com.epam.cloudx.dto.rs.Payload;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.List;
import java.util.stream.Collectors;
import software.amazon.awssdk.http.HttpStatusCode;
import software.amazon.awssdk.services.sns.SnsClient;
import software.amazon.awssdk.services.sns.model.PublishRequest;
import software.amazon.awssdk.services.sqs.SqsClient;
import software.amazon.awssdk.services.sqs.model.DeleteMessageRequest;
import software.amazon.awssdk.services.sqs.model.Message;
import software.amazon.awssdk.services.sqs.model.ReceiveMessageRequest;

public class QueueHandler implements RequestHandler<Object, ApiGatewayCompatibleResponse> {
    private static final SqsClient sqsClient = SqsClient.builder().region(REGION).build();
    private static final SnsClient snsClient = SnsClient.builder().region(REGION).build();
    private static final Gson gson = new GsonBuilder().setPrettyPrinting().create();

    @Override
    public ApiGatewayCompatibleResponse handleRequest(Object request, Context context) {
        //var lambdaLogger = context.getLogger();

        var receiveReq = ReceiveMessageRequest.builder()
                .queueUrl(NOTIFICATIONS_SQS_QUEUE_URL)
                .maxNumberOfMessages(RECEIVE_MAX_NUMBER)
                .waitTimeSeconds(RECEIVE_TIMEOUT_SECONDS)
                .build();

        var receiveResp = sqsClient.receiveMessage(receiveReq);
        var messages = receiveResp.messages();

        if (!messages.isEmpty()) {
            var imagesInfo = toImageInfo(messages);
            var notification = new Notification(imagesInfo);

            var publishReq = PublishRequest.builder()
                    .topicArn(NOTIFICATIONS_SNS_TOPIC_ARN)
                    .message(notification.getText())
                    .subject(notification.getSubject())
                    .build();

            snsClient.publish(publishReq);

            clearAll(messages);
        }

        var payload = new Payload(messages.size());
        return new ApiGatewayCompatibleResponse(HttpStatusCode.OK, payload);
    }

    private List<ImageInfo> toImageInfo(List<Message> messages) {
        return messages.stream()
                .map(m -> gson.fromJson(m.body(), ImageInfo.class))
                .collect(Collectors.toList());
    }

    private void clearAll(List<Message> messages) {
        messages.forEach(m -> sqsClient.deleteMessage(buildDeleteReq(m)));
    }

    private DeleteMessageRequest buildDeleteReq(Message msg) {
        return DeleteMessageRequest.builder()
                .queueUrl(NOTIFICATIONS_SQS_QUEUE_URL)
                .receiptHandle(msg.receiptHandle())
                .build();
    }
}
