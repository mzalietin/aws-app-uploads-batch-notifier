package com.epam.cloudx.dto.rs;

public class Payload {
    private final int messagesProcessed;

    public Payload(Integer messagesProcessed) {
        this.messagesProcessed = messagesProcessed;
    }

    public Integer getMessagesProcessed() {
        return messagesProcessed;
    }
}
