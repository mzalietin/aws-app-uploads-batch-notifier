package com.epam.cloudx.dto.rs;

import com.google.gson.Gson;
import java.util.HashMap;
import java.util.Map;

public class ApiGatewayCompatibleResponse {
    /**
     * Is payload Base64 encoded
     */
    private final boolean isBase64Encoded;

    /**
     * HTTP response status
     */
    private final int statusCode;

    /**
     * Custom headers in addition to standard ones
     */
    private final Map<String, String> headers;

    /**
     * Response payload
     */
    private final String body;

    public ApiGatewayCompatibleResponse(Integer statusCode, Object body) {
        this.isBase64Encoded = false;
        this.statusCode = statusCode;
        this.headers = null;
        this.body = new Gson().toJson(body);
    }

    public Boolean getIsBase64Encoded() {
        return isBase64Encoded;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    public Object getBody() {
        return body;
    }
}
