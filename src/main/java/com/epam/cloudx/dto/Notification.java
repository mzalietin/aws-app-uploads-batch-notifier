package com.epam.cloudx.dto;

import de.vandermeer.asciitable.AsciiTable;
import de.vandermeer.skb.interfaces.transformers.textformat.TextAlignment;
import java.nio.charset.StandardCharsets;
import java.util.List;

public class Notification {
    private static final String SUBJECT = "Content update in CloudxAwsApp";
    private static final String MAIL_TEMPLATE =
            "Dear user,\n\n"
            + "You received this message because you have subscribed to our content updates.\n"
            + "New images were published to the system: \n"
            + "%s\n\n"
            + "Thanks and have a nice day!";

    private final String text;

    public Notification(List<ImageInfo> images) {
        var sb = new StringBuilder();

        var header = new AsciiTable();
        header.addRule();
        header.addRow("Name", "Format", "Size (MB)");
        header.addRule();

        sb.append(toString(header)).append("\n");

        for (ImageInfo im: images) {
            var at = new AsciiTable();

            at.addRule();
            at.addRow(im.getName(), im.getExtension(), im.getSizeMb());
            at.addRule();

            sb.append(toString(at))
                    .append("\nDownload: ")
                    .append(im.getUrl())
                    .append("\n");
        }

        this.text = String.format(MAIL_TEMPLATE, sb);
    }

    private String toString(AsciiTable at) {
        var str = at.render(100);
        return new String(str.getBytes(StandardCharsets.UTF_8));
    }

    public String getSubject() {
        return SUBJECT;
    }

    public String getText() {
        return text;
    }
}
