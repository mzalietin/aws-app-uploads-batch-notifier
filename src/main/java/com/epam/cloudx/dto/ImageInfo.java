package com.epam.cloudx.dto;

public class ImageInfo {
    private String name;
    private String extension;
    private Float sizeMb;
    private String url;

    public ImageInfo() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public Float getSizeMb() {
        return sizeMb;
    }

    public void setSizeMb(Float sizeMb) {
        this.sizeMb = sizeMb;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
