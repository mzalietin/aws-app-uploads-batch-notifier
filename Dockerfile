FROM alpine:3.15

RUN apk add openjdk11

WORKDIR /function

COPY build/libs/aws-app-uploads-batch-notifier-*.jar ./
COPY build/dependency/*.jar ./

# configure the runtime startup as main
ENTRYPOINT [ "/usr/bin/java", "-cp", "./*", "com.amazonaws.services.lambda.runtime.api.client.AWSLambda" ]
# pass the name of the function handler as an argument to the runtime
CMD [ "com.epam.cloudx.QueueHandler::handleRequest" ]
