# Lambda function for `aws-app`

## Description

It pulls messages about images upload from SQS queue
and publishes notifications (emails) to SNS topic.

## Commands shorthand

+ build artifact

`gradlew build`

+ build Docker image

`docker build -t 524876846205.dkr.ecr.eu-central-1.amazonaws.com/aws-app-uploads-batch-notifier:latest .`

+ login to Docker repo

`aws ecr get-login-password --region eu-central-1 | docker login --username AWS --password-stdin 524876846205.dkr.ecr.eu-central-1.amazonaws.com`

+ push image

`docker push 524876846205.dkr.ecr.eu-central-1.amazonaws.com/aws-app-uploads-batch-notifier:latest`
